package com.mikbe.boozebot;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import com.mikbe.boozebot.recipes.Recipe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class ViewRecipeActivity extends Activity {
	
	BoozeApplication app;
	int recipeNumber;
	Recipe recipe;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_recipe);
		
		app = (BoozeApplication) getApplicationContext();		
		Intent intent = getIntent();
		recipeNumber = intent.getIntExtra(MainActivity.NEW_RECIPE, 0);
		recipe = app.getRecipes().get(recipeNumber);
		
		
		
		DecimalFormat formatter = new DecimalFormat("####");
		
		
		
		TextView name = (TextView) findViewById(R.id.recipe_name);
		name.setText(recipe.getName());
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		TextView ingredients = (TextView) findViewById(R.id.recipe_ingredients);
		String ingredientsStr = "";
		for ( int i=0; i<6; i++ ) {
			if ( recipe.getIngredients().get(i) > 0 ) {
				ingredientsStr += String.format("%4d",recipe.getIngredients().get(i)) + " ml " + Recipe.BOTTLES[i] + "\n";
			}
		}
		ingredients.setText(ingredientsStr);
	}
	
	public void makeRecipe(View view) {
		Intent intent = new Intent(this, MakeRecipeActivity.class);
		intent.putExtra(MainActivity.NEW_RECIPE, recipeNumber);
		startActivity(intent);
	}
	
	public void editRecipe(int recipeNumber) {
		Intent intent = new Intent(this, EditRecipeActivity.class);
    	intent.putExtra(MainActivity.NEW_RECIPE, recipeNumber);
    	startActivity(intent);
	}
	
	public void deleteRecipe(int recipeNumber) {
		Intent intent = new Intent(this, MainActivity.class);
		List<Recipe> recipes = app.getRecipes();
		recipes.remove(recipeNumber);
		try {
			app.persist();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_recipe, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_edit_recipe) {
			editRecipe(recipeNumber);
			return true;
		} else if (id == R.id.action_delete_recipe) {
			deleteRecipe(recipeNumber);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
