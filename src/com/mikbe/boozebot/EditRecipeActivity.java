package com.mikbe.boozebot;

import java.io.IOException;
import java.util.List;

import com.mikbe.boozebot.recipes.Recipe;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

public class EditRecipeActivity extends ListActivity {
	
	BoozeApplication app;
	Recipe recipe;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_recipe);
		
		Intent intent = getIntent();
		
		app = (BoozeApplication) getApplicationContext();
        List<Recipe> recipes = app.getRecipes();
		recipe = recipes.get(intent.getIntExtra(MainActivity.NEW_RECIPE, 0));
		
		System.out.println("Debug");
		System.out.println(recipe.getIngredients());
		
		EditText name = (EditText) findViewById(R.id.edit_name);
		name.setText(recipe.getName());
		name.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				recipe.setName(s.toString());
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		ListView listView = getListView();
		
		final EditIngredientAdapter adapter = new EditIngredientAdapter(this);
		adapter.updateIngredients(recipe.getIngredients());
		listView.setAdapter(adapter);
	}
	
	public class EditIngredientAdapter extends BaseAdapter {
		private List<Integer> ingredientList;
		private final Context context;
		
		public EditIngredientAdapter(Context context) {
			this.context = context;
		}
		
		public void updateIngredients(List<Integer> ingredientList) {
			this.ingredientList = ingredientList;
			notifyDataSetChanged();
		}
		
		@Override
		public int getCount() {
			return ingredientList.size();
		}
		
		@Override
		public Integer getItem(int position) {
			return ingredientList.get(position);
		}
		
		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public int getViewTypeCount() {
			return getCount();
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (true || convertView == null) {
				convertView = LayoutInflater.from(context)
						.inflate(R.layout.adapter_edit_ingredient, parent, false);
			}
			
			TextView nameView = (TextView) convertView.findViewById(R.id.ingredient_name);
			SeekBar progressView = (SeekBar) convertView.findViewById(R.id.ingredient_progress);
			final TextView displayView = (TextView) convertView.findViewById(R.id.ingredient_display);
			
			Integer ingredient = ingredientList.get(position);
			
			nameView.setText(Recipe.BOTTLES[position]);
			progressView.setProgress(ingredient);
			displayView.setText(ingredient + " ml");
			
			final int pos = position;
			
			progressView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					
				}
				
				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					
				}
				
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {
					ingredientList.set(pos, progress);
					displayView.setText(progress + " ml");
				}
			});
			
			return convertView;
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		try {
			app.persist();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_recipe, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
