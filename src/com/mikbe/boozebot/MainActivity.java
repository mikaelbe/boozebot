package com.mikbe.boozebot;

import java.util.List;

import com.mikbe.boozebot.recipes.Recipe;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends ListActivity {
	public final static String NEW_RECIPE = "com.mikbe.boozebot.NEWRECIPE";
	public final static String RANDOM_RECIPE = "com.mikbe.boozebot.RANDOMRECIPE";
	
	BoozeApplication app;
	List<Recipe> recipes;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	
    	app = (BoozeApplication) getApplicationContext();
        recipes = app.getRecipes();
        
        ListView listView = getListView();
        
        final RecipeAdapter adapter = new RecipeAdapter(this);
        adapter.updateRecipes(recipes);
        listView.setAdapter(adapter);
        
        listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int position,
					long arg3) {
				viewRecipe(position);
			}
        	
        });
    }
    
    public class RecipeAdapter extends BaseAdapter {
    	private List<Recipe> recipes;
    	private final Context context;
    	
    	public RecipeAdapter(Context context) {
    		this.context = context;
    	}
    	
    	public void updateRecipes(List<Recipe> recipes) {
    		this.recipes = recipes;
    		notifyDataSetChanged();
    	}
    	
    	@Override
    	public int getCount() {
    		return recipes.size();
    	}
    	
    	@Override
    	public Recipe getItem(int position) {
    		return recipes.get(position);
    	}
    	
    	@Override
    	public long getItemId(int position) {
    		return position;
    	}
    	
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
    		if (convertView == null) {
    			convertView = LayoutInflater.from(context)
    					.inflate(R.layout.adapter_recipes, parent, false);
    		}
    		
    		TextView nameView = (TextView) convertView.findViewById(R.id.name);
    		Recipe recipe = recipes.get(position);
    		nameView.setText(recipe.getName());
    		    		
    		return convertView;
    	}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
    	switch(item.getItemId()) {
    	case R.id.action_new_recipe:
    		openNewRecipe();
    		return true;
    	case R.id.action_settings:
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
    
    public void viewRecipe(int number) {
    	Intent intent = new Intent(this, ViewRecipeActivity.class);
    	intent.putExtra(NEW_RECIPE, number);
    	startActivity(intent);
    }
    
    public void randomRecipe(View view) {
    	Intent intent = new Intent(this, MakeRecipeActivity.class);
    	intent.putExtra(RANDOM_RECIPE, true);
    	startActivity(intent);
    }
    
    public void openNewRecipe() {
    	Intent intent = new Intent(this, EditRecipeActivity.class);
    	Recipe recipe = new Recipe();
    	app.addRecipe(recipe);
    	intent.putExtra(NEW_RECIPE, recipes.size() - 1);
    	startActivity(intent);
    }
}
