package com.mikbe.boozebot;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikbe.boozebot.recipes.Recipe;

import android.app.Application;
import android.content.Context;

public class BoozeApplication extends Application {
	private final static String FILENAME = "booze";
	
	private List<Recipe> recipes;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		System.out.println("Starting Booze Application");
		
		try {
			recipes = retrieve();
		} catch (IOException e) {
			recipes = new ArrayList<Recipe>();
			e.printStackTrace();
		}
	}
	
	public List<Recipe> getRecipes() {
		return recipes;
	}
	
	public void addRecipe(Recipe recipe) {
		recipes.add(recipe);
	}
	
	public void persist() throws IOException {
		FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_PRIVATE);
		fos.write((new Gson()).toJson(recipes).getBytes());
		fos.close();
	}
	
	private List<Recipe> retrieve() throws IOException {
		FileInputStream fis = openFileInput(FILENAME);
		InputStreamReader inputStreamReader = new InputStreamReader(fis);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		StringBuilder sb = new StringBuilder();
		String line;
		
		while ((line = bufferedReader.readLine()) != null) {
			sb.append(line);
		}
		
		Type collectionType = new TypeToken<List<Recipe>>(){}.getType();
		
		return (new Gson()).fromJson(sb.toString(), collectionType);
	}
}
