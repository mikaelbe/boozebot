package com.mikbe.boozebot;

import com.mikbe.boozebot.recipes.Recipe;
import com.silabs.hijack.DebugLogger;
import com.silabs.hijack.HijackActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MakeRecipeActivity extends HijackActivity {
	
	private static final String LOG_TAG = "HijackActivity";
	private static final int SEND_TIMES = 1; // send so many times per drink
	
	Recipe recipe;
	ProgressBar progress;
	TextView status;
	Handler handler = new Handler();
	
	Thread makeRecipeThread = new Thread(new Runnable() {
		
		public void run() {
			handler.post(new Runnable() {
                public void run() {
                	progress.setProgress(0);
        			status.setText("Waiting for bot ...");
                }
            });
			
			while (!isConnected) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			// Wait for bot to become ready
			// while () {
			//     ...
			// }
			
			handler.post(new Runnable() {
                public void run() {
                	progress.setProgress(20);
        			status.setText("Sending message");
                }
            });
			
			if (isConnected) {
				int[] serialized = recipe.hijackSerialize();
				System.out.println(serialized);
				
				for (int i = 0; i < SEND_TIMES; i++) {
					hijackSend(serialized);
				}
			}
			
			handler.post(new Runnable() {
                public void run() {
                	progress.setProgress(30);
        			status.setText("Message sent");
                }
            });
			
			// Wait for bot to finish pouring
			// while () {
			//     ...
			// }
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			handler.post(new Runnable() {
				public void run() {
					finished();
				}
			});
		}
	});
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_make_recipe);
		
		BoozeApplication app = (BoozeApplication) getApplicationContext();
		Intent intent = getIntent();
		recipe = app.getRecipes().get(intent.getIntExtra(MainActivity.NEW_RECIPE, 0));
		boolean random = intent.getBooleanExtra(MainActivity.RANDOM_RECIPE, false);
		
		if (random) {
			recipe = Recipe.randomRecipe();
			System.out.println("Random recipe");
			System.out.println(recipe.getIngredients());
		}
		
		TextView name = (TextView) findViewById(R.id.makeRecipeNameText);
		progress = (ProgressBar) findViewById(R.id.makeRecipeProgressBar);
		status = (TextView) findViewById(R.id.makeRecipeStatus);
		
		name.setText("Making " + recipe.getName());
		progress.setVisibility(View.VISIBLE);
		progress.setProgress(0);
		
		makeRecipeThread.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.make_recipe, menu);
		return true;
	}
	
	@Override
    public void messageReceived(int[] message) {
		
    }
	
	public void finished() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
