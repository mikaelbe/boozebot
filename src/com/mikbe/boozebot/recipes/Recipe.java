package com.mikbe.boozebot.recipes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Recipe {
	
	private String name;
	private List<Integer> ingredients;
	
	public static final String[] BOTTLES = new String[]{"Gin", "Tequila", "Vodka", "Tonic", "Rum", "Fanta/Cola"};
	
	public Recipe(String name, List<Integer> ingredients) {
		this.name = name;
		this.ingredients = ingredients;
	}
	
	public Recipe() {
		this.name = "New recipe";
		this.ingredients = new ArrayList<Integer>(Collections.nCopies(6, 0));
	}
	
	public static Recipe randomRecipe() {
		Float size = new Float(200.0);
		Random random = new Random();
		ArrayList<Float> floats = new ArrayList<Float>();
		ArrayList<Integer> ints = new ArrayList<Integer>();
		Float sum = new Float(0.0);
		
		// Number of ingredients [2, 6]
		float tmp = random.nextFloat();
		System.out.println(tmp);
		int numIngredients = (int) (2.0 + 4.0 * tmp);
		System.out.println(numIngredients);
		
		// Which ingredients
		boolean[] which = new boolean[6];
		int numAdded = 0;
		while(numAdded < numIngredients) {
			int randomBottle = (int) (6 * random.nextFloat());
			if (which[randomBottle] == false) {
				which[randomBottle] = true;
				numAdded++;
			}
		}
		
		// Generate 6 random numbers [0, 1), get the sum
		for (int i = 0; i < 6; i++) {
			if (which[i]) {
				Float num = random.nextFloat();
				floats.add(num);
				sum += num;
			} else {
				floats.add((float) 0.0);
			}
		}
		
		// Normalize and create ints
		for (int i = 0; i < 6; i++) {
			floats.set(i, floats.get(i) / sum * size);
			ints.add(floats.get(i).intValue());
		}
		
		return new Recipe("Random recipe", ints);
	}
	
	public List<Integer> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<Integer> ingredients) {
		this.ingredients = ingredients;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int[] hijackSerialize() {
		int[] ints = new int[6];
		
		for (int i = 0; i < 6; i++) {
			ints[i] = ingredients.get(i);
		}
		
		return ints;
	}

}
