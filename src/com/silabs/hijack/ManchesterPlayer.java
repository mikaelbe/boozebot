package com.silabs.hijack;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentLinkedQueue;


public class ManchesterPlayer {

    private static final short HARVESTING_AMPLITUDE = 32000;
    /** Handle to the AudioTrack object that interfaces the
     * sound hardware. */
    private AudioTrack audioTrack;

    /** This gives the amplitude of the generated waveform.
     *  A high value is desired to get a large amplitude,
     *  however setting this value too high have been observed
     *  to create som extra spikes on some phones. This is
     *  most likely due to the output driver not able to
     *  keep up. If this is changed it might be needed to
     *  change the ACMP threshold value on the EFM32.
     *  It is advised to use an oscilloscope to look at the actual
     *  waveform. */
    private static final short PCM_LEVEL = 32000;

    /** Number of PCM samples per second */
    private static final int AUDIO_SAMPLE_RATE = 44100;

    /** Baud rate. Must match LEUART configuration on EFM32.
     * Should also be an integer factor of the audio sample rate.
     * Here we are using 44100 / 5 = 8820 which gives 5 PCM samples
     * per 'half period' of the manchester encoded bit rate. */
    private static final int BAUD_RATE = 8820;

    /** The number of audio samples per LEUART baud. The actual bit rate is
     * half the LEUART baud rate. */
    private static final int HALF_PERIOD = AUDIO_SAMPLE_RATE / BAUD_RATE;

    // TODO comment
    private static final int SAMPLES_PER_BYTE = (10 * 2 * 2 * HALF_PERIOD);

    private static final int AUDIO_BUFFER_SIZE = 4096;


    private boolean useLeftForData = true;

    private boolean invertSignal = true;

    /** This flags is true if the app is using audio line harvesting, which
     * means a continuous wave must always be sent at the right channel. */
    private boolean audioLineHarvesting = false;

    private short[] harvestingWave = null;

    private boolean isActive = false;

    private ConcurrentLinkedQueue<short[]> messageQueue = null;


    public ManchesterPlayer()
    {
        this.audioTrack = null;
        messageQueue = new ConcurrentLinkedQueue<short[]>();

        harvestingWave = createZeroWave();
        if ( audioLineHarvesting )
        {
            harvestingWave = createHarvestingWave();
        }
    }

    public void setAudioLineHarvesting(boolean enable) {
        audioLineHarvesting = enable;
        if ( audioLineHarvesting )
        {
            harvestingWave = createHarvestingWave();
        }
        else
        {
            harvestingWave = createZeroWave();
        }
    }

    private void writeFrame(short[] buf, int index, int data) {
        int dataIndex = useLeftForData ? index : index+1;
        int harvestingIndex = useLeftForData ? index+1 : index;
        short writeData = invertSignal ? (short)-data : (short)data;

        buf[dataIndex] = writeData;

        writeHarvestingFrame(buf, harvestingIndex, index);
    }

    private void writeHarvestingFrame(short[] buf, int harvestingIndex, int index)
    {
        if ( audioLineHarvesting )
        {
            int x = (index/2) % 4; // TODO generalize
            buf[harvestingIndex] = (x < 2) ? HARVESTING_AMPLITUDE : -HARVESTING_AMPLITUDE;
        }
        else
        {
            buf[harvestingIndex] = 0;
        }
    }

    private short[] manchesterEncode(int[] data)
    {
        short[] pcm = new short[data.length * SAMPLES_PER_BYTE];

        int m=0;

        for (int i=0; i<data.length; i++)
        {
            // Start bit
            for ( int k=0; k<HALF_PERIOD; k++ )
            {
                writeFrame(pcm, m, -PCM_LEVEL);
                m += 2;
            }

            int d = data[i];

            for (int b=0; b<4; b++)
            {
                if ( ((d >> b) & 0x1) != 0 )
                {
                    for ( int j=0; j<HALF_PERIOD; j++ )
                    {
                        writeFrame(pcm, m, -PCM_LEVEL);
                        m += 2;
                    }
                    for ( int j=0; j<HALF_PERIOD; j++ )
                    {
                        writeFrame(pcm, m, PCM_LEVEL);
                        m += 2;
                    }
                }
                else
                {
                    for ( int j=0; j<HALF_PERIOD; j++ )
                    {
                        writeFrame(pcm, m, PCM_LEVEL);
                        m += 2;
                    }
                    for ( int j=0; j<HALF_PERIOD; j++ )
                    {
                        writeFrame(pcm, m, -PCM_LEVEL);
                        m += 2;
                    }
                }
            }

            // Stop bit
            for ( int k=0; k<HALF_PERIOD; k++ )
            {
                writeFrame(pcm, m, PCM_LEVEL);
                m += 2;
            }

            // Start bit
            for ( int k=0; k<HALF_PERIOD; k++ )
            {
                writeFrame(pcm, m, -PCM_LEVEL);
                m += 2;
            }


            for (int b=4; b<8; b++)
            {
                if ( ((d >> b) & 0x1) != 0 )
                {
                    for ( int j=0; j<HALF_PERIOD; j++ )
                    {
                        writeFrame(pcm, m, -PCM_LEVEL);
                        m += 2;
                    }
                    for ( int j=0; j<HALF_PERIOD; j++ )
                    {
                        writeFrame(pcm, m, PCM_LEVEL);
                        m += 2;
                    }
                }
                else
                {
                    for ( int j=0; j<HALF_PERIOD; j++ )
                    {
                        writeFrame(pcm, m, PCM_LEVEL);
                        m += 2;
                    }
                    for ( int j=0; j<HALF_PERIOD; j++ )
                    {
                        writeFrame(pcm, m, -PCM_LEVEL);
                        m += 2;
                    }
                }
            }

            // Stop bit
            for ( int k=0; k<HALF_PERIOD; k++ )
            {
                writeFrame(pcm, m, PCM_LEVEL);
                m += 2;
            }
        }

        return pcm;
    }

    public void startPlayer()
    {
        if ( isActive )
        {
            return;
        }

        isActive = true;

        new Thread(new Runnable() {
            @Override
            public void run() {
                int status;

                audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, 2 * AUDIO_BUFFER_SIZE, AudioTrack.MODE_STREAM);
                audioTrack.setStereoVolume(1.0f, 1.0f);
                audioTrack.play();

                short[] nextBuffer = null;
                int shortsLeft = 0;
                int currentPos = 0;
                int shortsWritten = 0;


                while(isActive)
                {
                    // If the current buffer has been successfully written, get the next buffer
                    // This will be either a new message, if one is ready
                    // or a harvesting wave (or zero wave if audio line harvesting is not used)
                    if ( shortsLeft == 0 ) {
                        nextBuffer = messageQueue.poll();

                        if (nextBuffer == null) {
                            nextBuffer = harvestingWave;
                        }
                        shortsLeft = nextBuffer.length;
                        currentPos = 0;
                    }

                    // Get the number of items to write. Cannot be larger than the buffer size.
                    int shortsToWrite = shortsLeft > AUDIO_BUFFER_SIZE ? AUDIO_BUFFER_SIZE : shortsLeft;

                    // Write the buffer to audio sink. This call blocks until all data
                    // has been written.
                    shortsWritten = audioTrack.write(nextBuffer, currentPos, shortsToWrite);
                    if ( shortsWritten < 0 )
                    {
                        //TODO handle error. raise exception here.
                    }

                    shortsLeft = shortsLeft - shortsWritten;
                    currentPos = currentPos + shortsWritten;
                }
            }
        }).start();
    }


    public void stopPlayer()
    {
        isActive = false;
    }

    private short[] createHarvestingWave()
    {
        short[] ret = new short[AUDIO_BUFFER_SIZE];

        int N = 2; // TODO generic frequency
        int index = 0;
        for ( int i=0; i<ret.length / (2 * N); i++ ) {
            for ( int j=0; j<N; j++ ) {
                ret[index++] = 0;  //TODO respect useLeftForData
                ret[index++] = (i % 2 == 0) ? HARVESTING_AMPLITUDE : -HARVESTING_AMPLITUDE;
            }
        }

        return ret;
    }

    private short[] createZeroWave()
    {
        short[] ret = new short[AUDIO_BUFFER_SIZE];
        for ( int i=0; i<ret.length; i++ ) {
            ret[i] = 0;
        }
        return ret;
    }

    public void send(int[] data)
    {
        int[] packetData = PacketEncoder.encodePacket(data);
        short[] pcm = manchesterEncode(packetData);
        messageQueue.add(pcm);
    }
}
