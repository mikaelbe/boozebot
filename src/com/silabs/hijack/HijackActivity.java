package com.silabs.hijack;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;

public class HijackActivity extends Activity implements ParserCallback {

    private static final String LOG_TAG = "HijackActivity";

    private HeadsetIntentReceiver headsetReceiver;
    private ManchesterParser parser;
    private ManchesterRecorder recorder;
    private ManchesterPlayer player;

    protected boolean isConnected;

    private AudioManager audioManager;
    private int savedVolume;

    public void hijackSend(final int[] message)
    {
        new Thread( new Runnable() {
            @Override
            public void run() {
                if ( isConnected && player != null ) {
                    player.send(message);
                }
            }
        }).run();
    }

    public void hijackClearReceiveBuffer() {
        if ( isConnected && parser != null ) {
            parser.restartParsing();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isConnected = false;
        headsetReceiver = new HeadsetIntentReceiver();
        audioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
    }

    @Override
    public void onResume() {
        IntentFilter plugFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        IntentFilter audioNoisyFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(headsetReceiver, plugFilter);
        registerReceiver(headsetReceiver, audioNoisyFilter);


        // Save volume and set to max when this app is running
        if ( audioManager != null ) {
            savedVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        unregisterReceiver(headsetReceiver);

        if ( parser != null ) {
            parser.stopParsing();
            parser = null;
        }
        if ( recorder != null ) {
            recorder.stopRecording();
            recorder = null;
        }
        if ( player != null ) {
            player.stopPlayer();
            player = null;
        }

        // Reset volume to what is was before
        if ( audioManager != null ) {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, savedVolume, 0);
        }

        super.onPause();
    }

    protected void onHijackConnected()
    {
        // A headset with mic is connected. Start parsing.
        DebugLogger.debug(LOG_TAG, "Headset is connected");

        try {
            // Start player
            player = new ManchesterPlayer();
            player.startPlayer();

            // Start parser
            parser = new ManchesterParser(this, this);
            parser.startParsing();

            // Start recorder
            recorder = new ManchesterRecorder(parser);
            recorder.startRecording();

            isConnected = true;
        } catch ( Exception e ) {
            DebugLogger.debug(LOG_TAG, "Error starting recording: " + e.getMessage());
            if (parser != null) {
                parser.stopParsing();
                parser = null;
            }
            if (recorder != null) {
                recorder.stopRecording();
                recorder = null;
            }
        }
    }

    protected void onHijackDisconnected()
    {
        isConnected = false;

        // A headset is disconnected or does not have a mic. Abort parsing.
        DebugLogger.debug(LOG_TAG, "Headset is disconnected");

        if ( parser != null ) {
            parser.stopParsing();
            parser = null;
        }
        if ( recorder != null ) {
            recorder.stopRecording();
            recorder = null;
        }
        if ( player != null ) {
            player.stopPlayer();
            player = null;
        }
    }

    @Override
    public void messageReceived(int[] message) {

    }


    private class HeadsetIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ( intent.getAction().equals(AudioManager.ACTION_AUDIO_BECOMING_NOISY) )
            {
                onHijackDisconnected();
            } else if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG))
            {
                int state = intent.getIntExtra("state", -1);
//                int mic = intent.getIntExtra("microphone", -1);

                if ( state == 1 )
                {
                    onHijackConnected();
                }
                else
                {
                    onHijackDisconnected();
                }
            }
        }
    }
}
