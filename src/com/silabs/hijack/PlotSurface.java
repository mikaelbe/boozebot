package com.silabs.hijack;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.LinkedList;


public class PlotSurface extends SurfaceView implements SurfaceHolder.Callback, Runnable {

    private static final int PLOT_MARGIN = 1;
    private static final int NUM_POINTS = 150;

    private static final int COLOR_METAL        = 0xff800000;
    private static final int COLOR_CAPACITIVE   = 0xff8800aa;

    private static final float MIN_VAL_METAL = 5f;
    private static final float MAX_VAL_METAL = 30f;
    private static final float MIN_VAL_CAPACITIVE = 4000f;
    private static final float MAX_VAL_CAPACITIVE = 7000f;

    private boolean renderThreadActive = false;

    private Thread renderThread = null;

    private SurfaceHolder surfaceHolder;
    private int surfaceWidth = 0;
    private int surfaceHeight = 0;

    private LinkedList<Integer> points;
    private Object listLock = new Object();

    private float minValue = 0f;
    private float maxValue = 0f;

    private Paint backgroundPaint;
    private Paint borderPaint;
    private Paint linePaint;




    public PlotSurface(Context context, AttributeSet attributes) {
        super(context, attributes);

        initPaint();

        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);

        points = new LinkedList<Integer>();

    }

    public boolean addPoint(DataSample sample)
    {
        synchronized (listLock)
        {
            points.addLast(sample.sampleValue);
            if (points.size() > NUM_POINTS)
            {
                points.removeFirst();
            }

            if ( sample.sampleValue > maxValue ) {
                maxValue = sample.sampleValue;
            }

            if ( sample.sampleValue < minValue ) {
                minValue = sample.sampleValue;
            }

            return true;
        }
    }

    public void setMetalSensor()
    {
        synchronized (listLock)
        {
            minValue = MIN_VAL_METAL;
            maxValue = MAX_VAL_METAL;
            linePaint.setColor(COLOR_METAL);
            points.clear();
        }
    }

    public void setCapacitiveSensor()
    {
        synchronized (listLock)
        {
            minValue = MIN_VAL_CAPACITIVE;
            maxValue = MAX_VAL_CAPACITIVE;
            linePaint.setColor(COLOR_CAPACITIVE);
            points.clear();
        }
    }

    private void startRenderThread()
    {
        if ( renderThread == null )
        {
            renderThread = new Thread(this);
            renderThreadActive = true;
            renderThread.start();
        }
    }

    private void stopRenderThread()
    {
        renderThreadActive = false;
        boolean retry = true;
        while (retry) {
            try {
                renderThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder)
    {
        startRenderThread();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height)
    {
        surfaceWidth = width;
        surfaceHeight = height;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder)
    {
        stopRenderThread();
    }

    private float yValueToCoord(int val)
    {
        float plotRange = surfaceHeight - 2 * PLOT_MARGIN;
        float valueRange = (maxValue - minValue) * 1.1f;

        float valueOffset = minValue;
        float coordOffset = surfaceHeight - PLOT_MARGIN;

        return coordOffset - ((val - minValue) / valueRange * plotRange);
    }

    private float xValueToCoord(int x)
    {
        float plotDomain = surfaceWidth - 2 * PLOT_MARGIN;
        return PLOT_MARGIN + (float)x / NUM_POINTS * plotDomain;
    }

    private void initPaint()
    {
        backgroundPaint = new Paint();
        backgroundPaint.setColor(0xFFb3b3b3);
        backgroundPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        linePaint = new Paint();
        linePaint.setColor(Color.RED);
        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setStrokeWidth(3.0f);

        borderPaint = new Paint();
        borderPaint.setColor(Color.BLACK);
        borderPaint.setStrokeWidth(2.0f);
        borderPaint.setStyle(Paint.Style.STROKE);
    }

    private void drawPlot(Canvas canvas)
    {
        if ( canvas == null )
        {
            return;
        }

        // Draw background
        canvas.drawRect(0, 0, surfaceWidth, surfaceHeight, backgroundPaint);

        // Draw graph
        synchronized (listLock)
        {
            Integer lp = null;
            int i = 0;
            for ( Integer p : points )
            {
                if ( lp != null )
                {
                    canvas.drawLine(xValueToCoord(i-1), yValueToCoord(lp), xValueToCoord(i), yValueToCoord(p), linePaint);
                }

                lp = p;
                i++;
            }
        }

        // Draw overlay border
        canvas.drawRect(0, 0, surfaceWidth, surfaceHeight, borderPaint);
    }

    @Override
    public void run() {
        while (renderThreadActive) {
            Canvas canvas = null;
            try {
                canvas = surfaceHolder.lockCanvas(null);
                synchronized (surfaceHolder)
                {
                    drawPlot(canvas);
                }
            } finally {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

}
