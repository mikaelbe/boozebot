package com.silabs.hijack;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

/**
 * Handles recording the raw audio stream and passing the recorded samples to the parser
 */
public class ManchesterRecorder {

    private static final int RECORDER_SAMPLERATE        = 44100;
    private static final int RECORDER_CHANNELS          = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING    = AudioFormat.ENCODING_PCM_16BIT;
    private static final int RECORDER_BUFFER_SIZE       = 1024;

    private AudioRecord recorder = null;
    private Thread recordingThread = null;
    private ManchesterParser parser = null;

    private boolean isRecording;

    public ManchesterRecorder(ManchesterParser parser)
    {
        this.parser = parser;
        this.isRecording = false;
    }


    /**
     * Starts the recording thread.
     */
    public void startRecording() throws Exception
    {
        int bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
                RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);

        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, bufferSize);

        if ( recorder.getState() != AudioRecord.STATE_INITIALIZED ) {
            throw new Exception("Failed to initialize AudioRecord instance");
        }

        recorder.startRecording();

        recordingThread = new Thread(new Runnable() {
            public void run() {
                recordLoop();
            }
        }, "ManchesterRecorder Thread");

        isRecording = true;
        recordingThread.start();
    }


    /**
     * Stops the recording thread.
     */
    public void stopRecording()
    {
        isRecording = false;

        if (null != recorder) {
            recorder.stop();
            recorder.release();

            try {
                recordingThread.join();
            } catch (InterruptedException e) {

            }

            recorder = null;
            recordingThread = null;
        }
    }

    /**
     * The recording loop. This function is run in the recording thread.
     * This function continuously records sample buffers from the raw audio
     * stream and passes these to the parsing thread.
     */
    public void recordLoop()
    {
        short sData[] = new short[RECORDER_BUFFER_SIZE];

        // TODO: better handling
        int count = 0;

        while (isRecording)
        {
            // Read raw audio data
            int n = recorder.read(sData, 0, RECORDER_BUFFER_SIZE);

            if ( n <= 0 ) {
                continue;
            }

            // When turning on the mic there is always a bit of noise
            // in the beginning. Discard first 10 sample buffers to ignore
            // these.
            if ( count < 10 ) {
                count++;
                continue;
            }

            // Convert the data to an Integer[] array
            Integer[] a = new Integer[n];
            for ( int i=0; i<n; i++ ) {
                a[i] = new Integer(sData[i]);
            }

            // Feed the samples to parsing thread
            if ( null != parser ) {
                parser.feed(a);
            }
        }
    }


}
