//package com.silabs.hijack;
//
//import android.app.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.media.AudioManager;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//
//public class PlotActivity extends HijackActivity {
//
//    private static final String LOG_TAG = "PlotActivity";
//
//    // Command protocol
//    private static final int CMD_SELECT_LCSENSE     = 1;
//    private static final int CMD_SELECT_CAPSENSE    = 2;
//
//    private static final int SENSOR_METAL = 1;
//    private static final int SENSOR_CAPACITIVE = 2;
//
//    // TODO: move to DataSample
//    private static final int DATA_SAMPLE_SIZE       = 9;
//
//    // Handle to surface view
//    private PlotSurface plotSurface;
//
//    // Handlers to GUI objects
//    private Button metalSensorButton;
//    private Button capacitiveSensorButton;
//    private ImageView legendView;
//    private ImageView connectionStatusView;
//
//    // State variables
//    private int wrongSensorCnt = 0;
//    private int curSensor = SENSOR_METAL;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setContentView(R.layout.plot);
//
//        // Get handles to UI elements
//        plotSurface = (PlotSurface)findViewById(R.id.plotSurface);
//        metalSensorButton = (Button)findViewById(R.id.buttonMetalSensor);
//        capacitiveSensorButton = (Button)findViewById(R.id.buttonCapsense);
//        legendView = (ImageView)findViewById(R.id.legendView);
//        connectionStatusView = (ImageView)findViewById(R.id.connectionStatusView);
//
//        // Set button listeners
//        metalSensorButton.setOnClickListener( new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                setMetalSensor(true);
//            }
//        });
//        capacitiveSensorButton.setOnClickListener( new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View view)
//            {
//                setCapacitiveSensor(true);
//            }
//        });
//
//        // Select metal sensor as default. Do not send switch command to MCU
//        setMetalSensor(false);
//    }
//
//    private void setMetalSensor(final boolean sendMsg)
//    {
//        metalSensorButton.setBackgroundResource(R.drawable.button_metal_pressed);
//        capacitiveSensorButton.setBackgroundResource(R.drawable.button_capacitive);
//        legendView.setImageResource(R.drawable.legend_metal_sensor);
//        plotSurface.setMetalSensor();
//
//        curSensor = SENSOR_METAL;
//
//        if ( sendMsg )
//        {
//            // Command format is simply one byte
//            // Value selects which sensor to monitor
//            int[] msg = new int [1];
//            msg[0] = CMD_SELECT_LCSENSE;
//            hijackSend(msg);
//
//            // Clear receive buffer to avoid receiving data
//            // from the previous sensor
//            hijackClearReceiveBuffer();
//        }
//
//    }
//
//    private void setCapacitiveSensor(final boolean sendMsg)
//    {
//        metalSensorButton.setBackgroundResource(R.drawable.button_metal);
//        capacitiveSensorButton.setBackgroundResource(R.drawable.button_capacitive_pressed);
//        legendView.setImageResource(R.drawable.legend_capactive_sensor);
//        plotSurface.setCapacitiveSensor();
//
//        curSensor = SENSOR_CAPACITIVE;
//
//        if ( sendMsg )
//        {
//            // Command format is simply one byte
//            // Value selects which sensor to monitor
//            int[] msg = new int [1];
//            msg[0] = CMD_SELECT_CAPSENSE;
//            hijackSend(msg);
//
//            // Clear receive buffer to avoid receiving data
//            // from the previous sensor
//            hijackClearReceiveBuffer();
//        }
//    }
//
//    private DataSample deserializeDataSample(int[] m)
//    {
//        if ( m.length != DATA_SAMPLE_SIZE )
//        {
//            return null;
//        }
//
//        DataSample ds = new DataSample();
//
//        // One byte: sensor id
//        ds.sensor = m[0];
//
//        // Four bytes: counter value
//        ds.counter = 0;
//        for ( int i=0; i<4; i++ ) {
//            ds.counter |= (m[1 + i] << (i * 8));
//        }
//
//        // Four bytes: sample value
//        ds.sampleValue = 0;
//        for ( int i=0; i<4; i++ ) {
//            ds.sampleValue |= (m[5 + i] << (i * 8));
//        }
//
//        return ds;
//    }
//
//    @Override
//    public void messageReceived(int[] message) {
//        super.messageReceived(message);
//
//        if ( message == null )
//        {
//            return;
//        }
//        DataSample v = deserializeDataSample(message);
//        if ( v != null )
//        {
//            if ( v.sensor != curSensor )
//            {
//                if ( ++wrongSensorCnt > 10 )
//                {
//                    if ( v.sensor == SENSOR_CAPACITIVE )
//                    {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                setCapacitiveSensor(false);
//                            }
//                        });
//
//                    } else if ( v.sensor == SENSOR_METAL )
//                    {
//                        runOnUiThread( new Runnable() {
//                            @Override
//                            public void run() {
//                                setMetalSensor(false);
//                            }
//                        });
//                    }
//                }
//            } else {
//                wrongSensorCnt = 0;
//                plotSurface.addPoint(v);
//            }
//
//        }
//        else
//        {
//            DebugLogger.error(LOG_TAG, "Invalid message found (" + message.length + " bytes)");
//        }
//    }
//
//    @Override
//    protected void onHijackConnected()
//    {
//        super.onHijackConnected();
//        connectionStatusView.setImageResource(R.drawable.con_connected);
//    }
//
//    @Override
//    protected void onHijackDisconnected()
//    {
//        super.onHijackDisconnected();
//        connectionStatusView.setImageResource(R.drawable.con_disconnected);
//    }
//
//    /*
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.plot, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }*/
//}
