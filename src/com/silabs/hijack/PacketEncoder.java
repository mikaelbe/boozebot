package com.silabs.hijack;


public class PacketEncoder {

    /** Version information in header */
    private static final int PACKET_HEADER_VER = 0x01;

    /** Maximum length of one message */
    private static final int MESSAGE_MAX_BYTES = 200;

    /** Size (in bytes) of the header field */
    private static final int HEADER_SIZE = 2;

    /** Size (in bytes) of the CRC field */
    private static final int CRC_SIZE = 2;

    /** Log tag for debug output */
    private static final String LOG_TAG = "PacketEncoder";


    private static int calcCrc(int[] byteArray, int len)
    {
        int crc = 0x0;

        for ( int i=0; i<len; i++ )
        {
            int data = byteArray[i];

            crc  = (crc >> 8) | (crc << 8);
            crc = crc & 0xFFFF;

            crc ^= data;
            crc = crc & 0xFFFF;

            crc ^= (crc & 0xff) >> 4;
            crc = crc & 0xFFFF;

            crc ^= crc << 12;
            crc = crc & 0xFFFF;

            crc ^= (crc & 0xff) << 5;
            crc = crc & 0xFFFF;
        }
        return crc;
    }

    public static int[] decode(int[] byteArr)
    {
        if (byteArr.length == 0) {
            DebugLogger.error(LOG_TAG, "Invalid packet received (0 bytes)");
            return null;
        }

        if (byteArr[0] != PACKET_HEADER_VER) {
            DebugLogger.error(LOG_TAG, "Unknown packet version: " + byteArr[0]);
            return null;
        }

        if (byteArr.length < 2) {
            DebugLogger.error(LOG_TAG, "Invalid packet received (1 byte)");
            return null;
        }
        int messageLength = byteArr[1];
        if (messageLength >= MESSAGE_MAX_BYTES) {
            DebugLogger.error(LOG_TAG, "Reported message length too long: " + messageLength);
            return null;
        }

        if (byteArr.length != HEADER_SIZE + messageLength + CRC_SIZE) {
            DebugLogger.error(LOG_TAG, "Packet length does not match header. Header: " + (HEADER_SIZE + messageLength + CRC_SIZE) + ". Actual: " + byteArr.length + ".");
            return null;
        }

        int reportedCrc = byteArr[HEADER_SIZE + messageLength] | (byteArr[HEADER_SIZE + messageLength + 1] << 8);

        int[] tmp = new int[HEADER_SIZE + messageLength];
        System.arraycopy(byteArr, 0, tmp, 0, HEADER_SIZE + messageLength);
        int calculatedCrc = calcCrc(byteArr, HEADER_SIZE + messageLength);

        if (reportedCrc != calculatedCrc) {
            DebugLogger.error(LOG_TAG, "CRC mismatch. Was: " + String.format("0x%04x", calculatedCrc)
                    + ". Should be: " + String.format("0x%04x", reportedCrc));
            return null;
        }

        int[] message = new int[messageLength];
        System.arraycopy(byteArr, HEADER_SIZE, message, 0, messageLength);

        return message;
    }

    public static int[] encodePacket(int[] message)
    {
        if ( message.length > MESSAGE_MAX_BYTES )
        {
            return null;
        }

        int[] p = new int[message.length + HEADER_SIZE + CRC_SIZE];

        p[0] = PACKET_HEADER_VER;
        p[1] = message.length;

        for ( int i=0; i<message.length; i++ )
        {
            p[i+HEADER_SIZE] = message[i];
        }

        int crc = calcCrc(p, HEADER_SIZE + message.length);
        p[HEADER_SIZE + message.length] = crc & 0xFF;
        p[HEADER_SIZE + message.length + 1] = (crc >> 8) & 0xFF;

        return p;
    }
}
