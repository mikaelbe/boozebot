package com.silabs.hijack;

import android.util.Log;

public class DebugLogger {

    private static final int LEVEL_NONE  = 0;
    private static final int LEVEL_ERROR = 1;
    private static final int LEVEL_DEBUG = 2;

    private static final int LOG_LEVEL = LEVEL_DEBUG;


    public static void debug(String tag, String message)
    {
        if ( LOG_LEVEL >= LEVEL_DEBUG )
        {
            Log.d(tag, message);
        }
    }

    public static void error(String tag, String message)
    {
        if ( LOG_LEVEL >= LEVEL_ERROR )
        {
            Log.e(tag, message);
        }
    }
}
