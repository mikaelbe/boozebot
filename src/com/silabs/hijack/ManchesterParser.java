package com.silabs.hijack;


import android.content.Context;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingDeque;


public class ManchesterParser {

    /* UART baud rate. Should match the LEUART configuration */
    private static final int UART_BAUD_RATE = 9000;

    /* Sample rate used by the mic ADC. Must match configuration in recorder. */
    private static final int AUDIO_SAMPLE_RATE = 44100;

    /** The ADC threshold for what is counted as 'activity', meaning packet data.
     *  This is an absolute value and represents change in count value from the current
     *  average. */
    private static final int ACTIVITY_THRESHOLD = 6000; //TODO: can this be made dynamic?

    /** Period threshold is used to differentiate between a short or long pulse.
     *  Any pulse longer than PERIOD_THRESHOLD samples is treated as a long pulse.  */
    private static final int PERIOD_THRESHOLD = AUDIO_SAMPLE_RATE / UART_BAUD_RATE + 2;

    /** Minimum number of consecutive samples below the ACTIVITY_THRESHOLD that should count
        as a 'quiet period' */
    private static final int SEARCH_QUIET_MIN = PERIOD_THRESHOLD * 50;

    /** Maximum number of samples to search for a 'quiet period'. If the search is exhausted
        the parser will throw an exception */
    private static final int SEARCH_QUIET_MAX = PERIOD_THRESHOLD * 1000;

    /** When parsing a packet, this many samples below ACTIVITY_THRESHOLD will abort parsing
        the current packet and enter 'wait for packet' state */
    private static final int QUIET_PERIOD_THRESHOLD = SEARCH_QUIET_MIN;

    private static final int PERIOD_MIN_THRESHOLD = 1;

    /** The length of the 'last samples' FIFO. This is used to calculate the local average. */
    private static final int LAST_SAMPLES_FIFO_LENGTH = AUDIO_SAMPLE_RATE / UART_BAUD_RATE;

    /** A tag used by the logging routines to identify log messages */
    private static final String LOG_TAG = "ManchesterParser";

    /** A thread-safe FIFO that stores sample buffers from the audio record thread. */
    private LinkedBlockingDeque<Integer[]> sampleList;

    /** Flag to check if parsing is currently enabled */
    private boolean isParsing;

    /** Handle to the parsing background thread */
    private Thread parsingThread;

    /** Current state of the parser */
    private ParserState state;

    /** Handles to the current sample buffers. The actual buffers
     *  go through these handles like a FIFO and all of them are saved
     *  for debugging in case of a parse error. */
    private Integer[] curSamples;
    private Integer[] nextCurSamples;
    private Integer[] prevCurSamples;

    /** Holds the current index (of curSamples[]) which indicates which sample we are parsing */
    private int curIndex;

    /** Two counters used by the parsing algorithm */
    private int counter;
    private int quietCounter;

    /** Flag indicating whether the current state of the parsed waveform is high or low. This is
     *  used by the algorithm to parse the waveform and detect errors. */
    private boolean lineHigh;

    /** A list of high/low periods (in number of samples). Each item represents a high or low period.
     *  The first period of a packet is always high (the LEUART start bit). */
    private LinkedList<Integer> periods;

    /** A 'last samples' FIFO. Contains the previous LAST_SAMPLES_FIFO_LENGTH samples and is used to
     *  calculate the local average. */
    private LinkedList<Integer> lastSamples = new LinkedList<Integer>();

    /** Context handle. Needed to access file system */
    private Context context;

    /** Handle to a callback interface which is used to notify the caller when
     * new messages have been received.  */
    private ParserCallback parserCallback;

    public ManchesterParser(Context context, ParserCallback callback)
    {
        this.context = context;
        sampleList = new LinkedBlockingDeque<Integer[]>();
        periods = new LinkedList<Integer>();
        parserCallback = callback;
    }

    /** Start parsing manchester encoded audio data in a new thread */
    public void startParsing() {
        parsingThread = new Thread( new Runnable() {
            public void run() {
                try {

                    initParsing();

                    while (isParsing) {
                        try {
                            prepareWaitForQuiet();
                            parseLoop();
                        } catch (ManchesterException e) {
                            DebugLogger.error(LOG_TAG, "Parsing error occurred: " + e.getMessage());
                        }
                    }
                } catch ( InterruptedException e ) {
                    e.printStackTrace();
                }
            }
        }, "ManchesterParser Thread");

        parsingThread.start();
    }

    /**
     * Stop the parsing algorithm
     */
    public void stopParsing() {
        isParsing = false;
        parsingThread.interrupt();
        try {
            parsingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void restartParsing() {
        new Thread( new Runnable() {
            @Override
            public void run() {
                stopParsing();
                startParsing();
            }
        }).run();
    }

    /**
     * Initialize a new parsing session. Must be run in the parsing thread.
     */
    private void initParsing() throws InterruptedException {
        isParsing = true;

        nextCurSamples = null;
        prevCurSamples = null;
        curSamples = null;
        counter = 0;

        nextCurSamples = sampleList.takeFirst();

        // Initialize last samples FIFO
        lastSamples.clear();
        for ( int i=0; i< LAST_SAMPLES_FIFO_LENGTH; i++ ) {
            lastSamples.addLast(nextCurSamples[0]);
        }

        curIndex = LAST_SAMPLES_FIFO_LENGTH;
    }

    /**
     * This function is called from the audio record thread to add new sample buffers
     */
    public void feed(Integer[] newSamples) {
        sampleList.addLast(newSamples);
    }

    /**
     * The parsing loop. Retrieves the sample buffers from sampleList and handles
     * the parsing algorithm state machine.
     *
     * @throws InterruptedException
     * @throws ManchesterException
     */
    private void parseLoop() throws InterruptedException, ManchesterException
    {
        DebugLogger.debug(LOG_TAG, "Starting parsing...");

        while (isParsing) {

            // Get the next sample buffer. The takeFirst() function is thread-safe
            // and will block if sampleList is empty. The record thread will
            // populate sampleList with audio samples.
            if ( curSamples == null || curIndex >= curSamples.length ) {
                // This call blocks
                Integer[] t = sampleList.takeFirst();

                // Keep both next and previous sample buffer in memory
                // Used for error logging and look-ahead
                prevCurSamples = curSamples;
                curSamples = nextCurSamples;
                nextCurSamples = t;

                curIndex = 0;
            }

            // Parsing algorithm consists of four states. Call the corresponding function.
            switch(state) {
                case waitingForQuiet:
                    waitForQuiet();
                    break;
                case waitingForPacket:
                    waitForPacket();
                    break;
                case measurePeriods:
                    measurePeriods();
                    break;
                case calculateBytes:
                    calculateBytes();
                    break;
            }
        }
    }


    /**
     * Called to switch to the 'wait for quiet' state.
     */
    private void prepareWaitForQuiet()
    {
        state = ParserState.waitingForQuiet;
        quietCounter = 0;
        counter = 0;
        lineHigh = false;
    }


    /**
     * Parsing algorithm for the 'wait for quiet' state. In this state
     * the algorithm waits for the line to be quiet/stable in order to
     * be sure we start parsing on the beginning of a packet.
     *
     * @throws ManchesterException
     */
    private void waitForQuiet() throws ManchesterException
    {
        while (curIndex < curSamples.length) {

            // Calculate current average
            int avg = 0;
            for ( Integer s : lastSamples ) {
                avg += s.intValue();
            }
            avg /= LAST_SAMPLES_FIFO_LENGTH;


            if (Math.abs(curSamples[curIndex] - avg) < ACTIVITY_THRESHOLD) {

                // If enough 'quiet' samples are found, enter the 'wait for packet' state
                quietCounter++;
                if ( quietCounter >= SEARCH_QUIET_MIN ) {
                    prepareWaitForPacket();
                    break;
                }
            } else {

                // Reset quiet counter if there is activity on the line
                quietCounter = 0;

                // Trigger error if a quiet period cannot be found
                if ( counter >= SEARCH_QUIET_MAX ) {
                    dumpSamples();
                    throw new ManchesterException("No quiet period found");
                }
            }


            // Update last samples FIFO
            lastSamples.removeFirst();
            lastSamples.addLast(curSamples[curIndex]);

            counter++;
            curIndex++;
        }
    }


    /**
     * Called to enter the 'wait for packet' state
     */
    private void prepareWaitForPacket()
    {
        state = ParserState.waitingForPacket;
    }


    /**
     * Parsing algorithm for the 'wait for packet' state. In this state
     * we wait until we find a high period that indicates the start of a packet.
     *
     * @throws ManchesterException
     */
    private void waitForPacket() throws ManchesterException
    {
        while ( curIndex < curSamples.length ) {

            // Calculate current average
            int avg = 0;
            for ( Integer s : lastSamples ) {
                avg += s.intValue();
            }
            avg /= LAST_SAMPLES_FIFO_LENGTH;

            // Check if current sample is a positive edge and enter the 'measure periods' state
            if ( curSamples[curIndex] - avg > ACTIVITY_THRESHOLD ) {
                prepareMeasurePeriods();
                DebugLogger.debug(LOG_TAG, "Packet start found at " + curIndex);
                return;
            }

            // Update last samples FIFO
            lastSamples.removeFirst();
            lastSamples.addLast(curSamples[curIndex]);

            curIndex++;
        }
    }

    /**
     * Called to enter the 'measure periods' state
     */
    private void prepareMeasurePeriods()
    {
        state = ParserState.measurePeriods;

        // Clear the periods array
        periods.clear();
        quietCounter = 0;
        counter = 0;

        // Line should always start high (the start condition)
        lineHigh = true;
    }

    /**
     * The 'measure periods' state of the parsing algorithm. In this state
     * the period of each pulse (high and low) are measured and stored.
     * A positive/negative edge is detected by comparing the current value
     * against the average of the 'last samples FIFO'. This improves sensitivity
     * by filtering out low frequency components that are superimposed because
     * of variations in the DC voltage.
     *
     * @throws ManchesterException
     */
    private void measurePeriods() throws ManchesterException
    {
        while ( curIndex < curSamples.length ) {

            // Calculate current average
            int avg = 0;
            for ( Integer s : lastSamples ) {
                avg += s.intValue();
            }
            avg /= LAST_SAMPLES_FIFO_LENGTH;

            // Check if the line is quiet and abort transfer if it is
            if ( Math.abs(curSamples[curIndex] - avg) < ACTIVITY_THRESHOLD ) {
                quietCounter++;
                if ( quietCounter >= QUIET_PERIOD_THRESHOLD ) {

                    // The line should always be low at the end of a packet.
                    // The last period is not detected by the algorithm because the
                    // line drops to idle instead of being pulled high
                    // Here we add a short period manually
                    if ( !lineHigh ) {
                        periods.addLast(PERIOD_THRESHOLD - 1);
                    } else {
                        // Line went idle while high. This is an error condition (stop bit is always low).
                        // Throw error and discard current packet
                        dumpSamples();
                        throw new ManchesterException("Line went from high to idle. CurIndex = " + curIndex);
                    }

                    prepareCalculateBytes();
                    return;
                }
            } else {
                quietCounter = 0;
            }

            // Get the signal level
            if ( lineHigh )
            {
                if ( avg - curSamples[curIndex] > ACTIVITY_THRESHOLD ) {

                    // Level change. Store period
                    lineHigh = false;
                    periods.addLast(counter);
                    counter = 1;

                } else {

                    // Line is stable
                    counter++;
                }
            } else {
                if ( curSamples[curIndex] - avg > ACTIVITY_THRESHOLD ) {

                    // Level change. Store period
                    lineHigh = true;
                    periods.addLast(counter);
                    counter = 1;

                } else {

                    // Line is stable
                    counter++;
                }
            }

            // Update last samples FIFO
            lastSamples.removeFirst();
            lastSamples.addLast(curSamples[curIndex]);

            curIndex++;
        }
    }

    /**
     * Called to enter the 'calculate bytes' state
     */
    private void prepareCalculateBytes()
    {
        state = ParserState.calculateBytes;
    }

    /**
     * The 'calculate bytes' state of the parsing algorithm. This function
     * translates the recorded waveform (list of high and low periods) into
     * the actual bytes in the message.
     *
     * The data is manchester encoded with a few modifications. Every data packet
     * is separated by a 'quiet period', where there is no activity on the line.
     * The input to this function is a list of pulse widths. Each entry in the
     * periods array represents a high-to-low or low-to-high transition. Each
     * pulse width is characterized as being either 'short' or 'long'.
     *
     *  - Each packet starts with a high period. This can be either high
     *    or low. The first 'short' part of this pulse is simply the start
     *    condition and conveys no data. The first bit period starts immediately
     *    after the start condition.
     *  - In the middle of each bit period there is always one transition. If this
     *    is high-to-low, the bit is a '1'. If the transition is low-to-high it is '0'
     *  - After each 4th bit period there is a bit period with a low-to-high transition
     *    that must be ignored (this is the stop and start condition from LEUART between
     *    each UART byte).
     *  - At the end of each packet the stop condition is represented by a low short period
     *
     *
     * Bytes are sent LSB first and each packet has a fixed byte length. If the algorithm
     * detects a shorter or longer packet, it is discarded and an error is thrown.
     *
     * @throws ManchesterException
     */
    private void calculateBytes() throws ManchesterException
    {
        // Counter used to keep track of bit periods. Some transitions will be at
        // the edge (start/end) of a bit period, while other will be in the middle
        // of a bit period. When h mod 2 == 0, the current
        // transition is in the middle of a bit period.
        int h = 0;

        // Flag telling if the current line state is high or low
        boolean high = false;

        // Storage for the parsed bits and bytes
        ArrayList<Integer> parsedBits = new ArrayList<Integer>();
        ArrayList<Integer> parsedBytes = new ArrayList<Integer>();

        int index = 0;

        // If there are no recorded periods, stop here
        if ( periods.size() == 0 )
        {
            prepareWaitForQuiet();
            return;
        }

        int sampleCount = 0;

        // Parse all the bits in the packet
        while (!periods.isEmpty())
        {
            int p = periods.pollFirst();

            // In the middle of a bit period
            if ( h % 2 == 0 )
            {
                if ( index > 0 ) // Skip the start bit
                {
                    // Transition high to low => 1
                    // Transition low to high => 0
                    if ( high ) {
                        parsedBits.add(1);
                    } else {
                        parsedBits.add(0);
                    }
                }

                // This test allows to very short pulses that are most likely noise
                if ( p < PERIOD_MIN_THRESHOLD ) {
                    dumpSamples();
                    throw new ManchesterException("Too short pulse detected");
                }

                // Update h according to the pulse length
                if ( p > PERIOD_THRESHOLD ) {
                    h = h + 2;
                } else {
                    h = h + 1;
                }

            // At the edge of a bit period
            } else {
                if ( p > PERIOD_THRESHOLD ) {
                    if ( index == 0 )  {
                        // Special case for the start bit
                        h = h + 2;
                    } else {
                        // A long pulse was found at the start of a bit period. This represents
                        // a parse error as there must always be a transition in the middle
                        // of a bit period.
                        dumpSamples();
                        throw new ManchesterException("Parse error. CurIndex = " + curIndex +
                                ", sampleCount = " + sampleCount + ", p = " + p + ", periods.size() = " + periods.size());
                    }
                } else {
                    h = h + 1;
                }
            }

            index++;
            high = !high;

            // Keep track of which sample we are parsing
            sampleCount += p;
        }

        // Loop through the parsed bits and calculate and store each byte
        int shifts = 0;
        int byteValue = 0;
        int i = 0;
        for ( Integer bit : parsedBits )
        {
            // Skip Start/Stop bits from LEUART
            i++;
            if ( i % 5 == 0 && i > 1 && i < parsedBits.size()  )
            {
                continue;
            }

            // Calculate byte value
            byteValue |= bit << shifts;

            // Finished parsing one byte. Store it in array.
            if ( ++shifts >= 8 ) {
                shifts = 0;
                parsedBytes.add(byteValue);
                byteValue = 0;
            }
        }

        // No bytes found
        if ( parsedBytes.size() == 0 ) {
            dumpSamples();
            throw new ManchesterException("Non-message noise found!");
        }

        // Store bytes in an int[] array and print debug information
        String debugStr = "Received " + parsedBytes.size() + " bytes:";
        int[] recvArray = new int[parsedBytes.size()];
        int recvIndex = 0;
        for ( Integer b : parsedBytes ) {
            debugStr += " " + b;
            recvArray[recvIndex++] = b.intValue();
        }
        debugStr += "\n";
        DebugLogger.debug(LOG_TAG, debugStr);

        parsePacket(recvArray);

        // Enter the 'wait for quiet' state to prepare for next packet
        prepareWaitForQuiet();
    }

    private void parsePacket(int[] byteArr)
    {
        if ( parserCallback != null )
        {
            parserCallback.messageReceived(PacketEncoder.decode(byteArr));
        }
    }




    /**
     * Debug function. Saves the current, previous and next raw sample buffers.
     */
    private void dumpSamples() {
        if (curSamples != null) {
            saveSamples(curSamples, "cursamples.txt");
        }

        if ( prevCurSamples != null ) {
            saveSamples(prevCurSamples, "prevsamples.txt");
        }

        if ( nextCurSamples != null ) {
            saveSamples(nextCurSamples, "nextsamples.txt");
        }
    }

    /**
     * Called by parsing thread to save raw samples for debug purposes.
     * Samples are stored in a text file. One line per sample.
     *
     * @param curSamples A sample buffer
     * @param filename File name to save buffer
     */
    public void saveSamples(Integer[] curSamples, String filename )
    {
        try {
            // Open file in application directory
            File logFile = new File(context.getFilesDir() + "/" + filename);
            DebugLogger.debug(LOG_TAG, "Saving " + curSamples.length + " samples to " + logFile.getAbsolutePath());

            // Save to disk
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, false));
            for ( Integer s : curSamples ) {
                buf.write("" + s + "\n");
            }
            buf.close();

            // Make sure the file is globally readable
            logFile.setReadable(true, false);

        }
        catch ( IOException e ) {
            e.printStackTrace();
            // TODO: handle error?
        }
    }


    /**
     * Custom exception for this class. This exception is thrown on any parse
     * error.
     */
    public static class ManchesterException extends Exception {
        public ManchesterException(String msg)
        {
            super(msg);
        }
    }

    /**
     * Definition of the four parsing states.
     */
    private enum ParserState {
        waitingForQuiet,
        waitingForPacket,
        measurePeriods,
        calculateBytes,
    }
}
