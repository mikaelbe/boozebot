package com.silabs.hijack;

/**
 * Created by fidovlan on 6/4/14.
 */
public interface ParserCallback {

    void messageReceived(int[] message);
}
